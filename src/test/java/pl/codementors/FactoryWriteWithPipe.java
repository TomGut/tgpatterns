package pl.codementors;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Interfaces.WriteFactory;

public class FactoryWriteWithPipe implements WriteFactory {
    @Override
    public PersonWriter get() {
        return new PersonWriteImp2();
    }
}
