package pl.codementors;

import pl.codementors.Models.Adress;
import pl.codementors.Models.Person;

public class PersonBuilder {

    private String name;
    private Adress adress;

    public PersonBuilder withName(String name){
        this.name = name;
        return this;
    }

    public PersonBuilder withAdress(Adress adress){
        this.adress = adress;
        return this;
    }

    public Person build(){
        Person p = new Person();
        p.setName(name);
        p.setAdress(adress);

        return p;
    }
}
