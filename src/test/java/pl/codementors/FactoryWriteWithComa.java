package pl.codementors;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Interfaces.WriteFactory;

public class FactoryWriteWithComa implements WriteFactory {

    @Override
    public PersonWriter get() {
        return new PersonWiterImp1();
    }
}
