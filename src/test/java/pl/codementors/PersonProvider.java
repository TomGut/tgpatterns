package pl.codementors;

import pl.codementors.Models.Adress;
import pl.codementors.Models.Person;

public class PersonProvider {

    public Person getNewPerson(){

        Adress adress = new Adress("Jeżewska", 12);

        PersonBuilder builder = new PersonBuilder();
        builder.withName("Edek").withAdress(adress);

        return builder.build();
    }
}
