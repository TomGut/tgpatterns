package pl.codementors;

import pl.codementors.Interfaces.WriteFactory;
import pl.codementors.Models.Person;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        PersonProvider buildPerson = new PersonProvider();
        Person person = buildPerson.getNewPerson();
        System.out.println(person);

        PersonWiterImp1 imp1 = new PersonWiterImp1();
        imp1.write(person);

        PersonWriteImp2 imp2 = new PersonWriteImp2();
        imp2.write(person);

        Scanner scan = new Scanner(System.in);
        System.out.println();

        WriteFactory write = null;
        boolean running = true;

        while (running){
            System.out.println("Podaj czy chcesz wypisać info z \n 1 -przecinkami \n 2 - z pipe \n 3 - wyjście");
            System.out.println();
            String userInput = scan.nextLine();

            if(userInput.equals("1")){
                write = new FactoryWriteWithComa();
                System.out.println();
                write.get().write(person);
                System.out.println();
            }else if(userInput.equals("2")){
                write = new FactoryWriteWithPipe();
                System.out.println();
                write.get().write(person);
                System.out.println();
            }else if(userInput.equals("3")){
                System.out.println("BYE");
                break;
            }
        }
    }
}
