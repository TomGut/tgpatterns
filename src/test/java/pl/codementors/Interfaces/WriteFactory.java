package pl.codementors.Interfaces;

import pl.codementors.Interfaces.PersonWriter;

public interface WriteFactory {
    PersonWriter get();
}
