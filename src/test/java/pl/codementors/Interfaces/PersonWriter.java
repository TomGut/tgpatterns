package pl.codementors.Interfaces;

import pl.codementors.Models.Person;

public interface PersonWriter {

    void write(Person person);
}
