package pl.codementors.Models;

public class Adress {

    private String street;
    private int homeNumber;

    public Adress(){

    }

    public Adress(String street, int homeNumber){
        this.street = street;
        this.homeNumber = homeNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(int homeNumber) {
        this.homeNumber = homeNumber;
    }

    @Override
    public String toString() {
        return "Adress{" + "street='" + street + '\'' + ", homeNumber=" + homeNumber + '}';
    }
}
