package pl.codementors.Models;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Interfaces.WriteFactory;
import pl.codementors.PersonWiterImp1;

public class WriteWithComma implements WriteFactory {
    @Override
    public PersonWriter get() {
        return new PersonWiterImp1();
    }
}
