package pl.codementors.Models;


public class Person {

    private String name;
    private Adress adress;

    public Person(){

    }


    public Person(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Person{" + "name='" + name + '\'' + ", adress=" + adress + '}';
    }
}
