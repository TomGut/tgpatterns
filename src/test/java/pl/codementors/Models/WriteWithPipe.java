package pl.codementors.Models;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Interfaces.WriteFactory;
import pl.codementors.PersonWriteImp2;

public class WriteWithPipe implements WriteFactory {

    @Override
    public PersonWriter get() {
        return new PersonWriteImp2();
    }
}
