package pl.codementors;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Interfaces.WriteFactory;

public class Application {

    private WriteFactory writer;
    private PersonProvider person;

    public Application(WriteFactory writer, PersonProvider person) {
        this.writer = writer;
        this.person = person;
    }

    public void start(){
       PersonWriter type = writer.get();
       type.write(person.getNewPerson());
    }
}
