package pl.codementors;

import pl.codementors.Interfaces.PersonWriter;
import pl.codementors.Models.Person;

public class PersonWriteImp2 implements PersonWriter {

    @Override
    public void write(Person person) {
        System.out.println(person.getName() + " | " + person.getAdress().getStreet() + " | " + person.getAdress().getHomeNumber());
    }
}
